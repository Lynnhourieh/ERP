<?php

use Illuminate\Http\Request;
use App\Http\Controllers\teams;
use App\Http\Controllers\sysrole;


use App\Http\Controllers\employees;
use Illuminate\Support\Facades\Route;


Route::resource('/team',teams::class);
Route::resource('/sysrole',sysrole::class);
Route::resource('/employee',employees::class);
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::get('/posts', function () {
//     return response()->json([
//         'posts' => [
//             'title' => 'Post is done'
//         ]
//     ]);
// });



Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});
