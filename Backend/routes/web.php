<?php

use App\Models\Listing;
use Illuminate\Http\Request;


//to connect  the class named Test in Listing.php with web.php
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PagesController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//All Listings
// Route::get('/', function () {
//     return view('listings', [
//         'headings' => "latest listings",
//         //class is called Listing is in Listing.php in Models;
//         //::all() is taking all the data;
//         'listings'=>Listing::all()
//     ]);
// });

//Single Listing
// Route::get('/listings/{id}', function ($id) {
//     return view('listing', [
//         //class is called Listing;
//         //::find() is finding data acc to id;
//         'listing'=>Listing::find($id)
//     ]);
// });


// Route::get('/hello', function () {
//     return response('<h1>welcome ya hala</h1>');
// });

// Route::get('/posts/{id}', function ($id) {
//     // dd($id);
//     return response('POST '.$id);
// })->where('id','[0-9]+');


// Route::get('/search', function (Request $request) {
//     return $request->name ." ".$request->city;
// });


// Route::get('/test', [PagesController::class,'index']);
